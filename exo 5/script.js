window.addEventListener("DOMContentLoaded", () => {
  const images = document.getElementsByTagName("img");
  const imgContainer = document.getElementById("image-container");
  const overlay = document.getElementById("overlay");
  const btn = document.getElementsByClassName("close");
  const widths = [];
  for (img of images) {
    widths.push(img.width);
  }

  for (let i = 0; i < images.length; i++) {
    images[i].style.width = "150px";
    images[i].style.height = "150px";
  }

  window.addEventListener("click", (e) =>
    e.target === overlay ? overlay.classList.add("hidden") : false
  );

  btn[0].addEventListener("click", () => {
    overlay.classList.add("hidden");
  });

  images[0].addEventListener("click", () => {
    overlay.classList.remove("hidden");
    imgContainer.classList.remove("portrait");
    imgContainer.classList.remove("paysage");
    if (widths[0] === 1600) {
      imgContainer.classList.add("paysage");
    } else {
      imgContainer.classList.add("portrait");
    }
    imgContainer.innerHTML = `<img id="affiche" src="./images/1.jpg" alt="1">`;
  });

  images[1].addEventListener("click", () => {
    overlay.classList.remove("hidden");
    imgContainer.classList.remove("portrait");
    imgContainer.classList.remove("paysage");
    if (widths[1] === 1600) {
      imgContainer.classList.add("paysage");
    } else {
      imgContainer.classList.add("portrait");
    }
    imgContainer.innerHTML = `<img id="affiche" src="./images/2.jpg" alt="2">`;
  });

  images[2].addEventListener("click", () => {
    overlay.classList.remove("hidden");
    imgContainer.classList.remove("portrait");
    imgContainer.classList.remove("paysage");
    if (widths[2] === 1600) {
      imgContainer.classList.add("paysage");
    } else {
      imgContainer.classList.add("portrait");
    }
    imgContainer.innerHTML = `<img id="affiche" src="./images/3.jpg" alt="3">`;
  });

  images[3].addEventListener("click", () => {
    overlay.classList.remove("hidden");
    imgContainer.classList.remove("portrait");
    imgContainer.classList.remove("paysage");
    if (widths[3] === 1600) {
      imgContainer.classList.add("paysage");
    } else {
      imgContainer.classList.add("portrait");
    }
    imgContainer.innerHTML = `<img id="affiche" src="./images/4.jpg" alt="4">`;
  });


  // le bonus ne marche pas....
  document.addEventListener("keyup", (e) => {
    if (e.key === "ArrowRight" || e.key === "Right") {
      const imgAffiche = document.getElementById("affiche");
      key = imgAffiche.alt;
      imgContainer.innerHTML = `<img id="affiche" src="./images/${(key+1)%4}.jpg" alt="${(key+1)%4}">`;
    } else if (e.key === "ArrowLeft" || e.key === "Left") {
      const imgAffiche = document.getElementById("affiche");
      key = imgAffiche.alt;
      imgContainer.innerHTML = `<img id="affiche" src="./images/${(key-1)%4}.jpg" alt="${(key-1)%4}">`;
    }
  });
});
