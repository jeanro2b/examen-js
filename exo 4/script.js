window.addEventListener("DOMContentLoaded", () => {
  const presentation = document.getElementById("presentation");
  const information = document.getElementById("information");
  const moncv = document.getElementById("moncv");
  const contact = document.getElementById("contact");
  const nav = document.getElementsByTagName("a");

  const main = [presentation, information, moncv, contact];

  nav[0].addEventListener('click', () => {
    for (j = 0; j < nav.length; j++) {
      nav[j].classList.remove("current");
      console.log(main[j]);
      main[j].classList.remove("current");
    }
    main[0].classList.add("current");
    nav[0].classList.add("current");
  });

  nav[1].addEventListener('click', () => {
    for (j = 0; j < nav.length; j++) {
      nav[j].classList.remove("current");
      console.log(main[j]);
      main[j].classList.remove("current");
    }
    main[1].classList.add("current");
    nav[1].classList.add("current");
  });

  nav[2].addEventListener('click', () => {
    for (j = 0; j < nav.length; j++) {
      nav[j].classList.remove("current");
      console.log(main[j]);
      main[j].classList.remove("current");
    }
    main[2].classList.add("current");
    nav[2].classList.add("current");
  });

  nav[3].addEventListener('click', () => {
    for (j = 0; j < nav.length; j++) {
      nav[j].classList.remove("current");
      console.log(main[j]);
      main[j].classList.remove("current");
    }
    main[3].classList.add("current");
    nav[3].classList.add("current");
  });

});
