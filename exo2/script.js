const tabNum1 = [4, 8, 15, 16, 23, 42];
const tabNum2 = [65, 32, 456, 12, 64, 23, 94, 12];

window.addEventListener("DOMContentLoaded", () => {
  for (let i = 0; i < tabNum1.length; i++) {
    for (let j = 0; j < tabNum2.length; j++) {
      const section = document.createElement("section");
      section.innerText = `${tabNum1[i]} x ${tabNum2[j]} = ${
        tabNum1[i] * tabNum2[j]
      }`;
      console.log(section);
      document.body.appendChild(section);
    }
  }
});
