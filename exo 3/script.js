const addEventToElement = (targetName, eventType, listener) => {
  const target = document.getElementsByTagName(`${targetName}`);
  window.addEventListener("DOMContentLoaded", () => {
    target[0].addEventListener(eventType, listener);
  });
};

addEventToElement("button", "click", () => {
  console.log("OK");
});
